#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

using namespace std;

/* Estrucutura con atributos para el manejo de archivos */
struct Archivo {
  char * nombre;
  int lineas;
  int palabras;
  int caracteres;
};
/* Función recibe el putero al archivo para abrirlo y 
contar las caracteristicas de interes */
void * abrirArchivo(void * archivo) {
  /* Se declaran vaariables para el control de la posición de la cuenta 
  en el archivo, se inicializan en 0 */
  Archivo * str = (Archivo * ) archivo;
  char c[2], * last_c;
  int lineas = 0, palabras = 0, caracteres = 0;
  bool despues_linea;

  /* Se abre el archivo */
  int stream = open(str -> nombre, O_RDONLY);
  /* Da cuenta de un error al usuario cuando falla al abrir el archivo */
  if (stream == -1) {
    cout << "Error al abrir archivo" << endl;
    pthread_exit(0);
  }
  /* Se utulizan ciclos para contar los caracteres, lineas y palabras */
  while (read(stream, c, 1)) {
    caracteres++;
    last_c = c; 
    if (despues_linea) {
      palabras++;
      despues_linea = false; 
      despues_linea = false;
    }
    if (strchr("\n", c[0])) {
      lineas++;
      despues_linea = true;
    }
    if (strchr(" ", c[0])) {
      palabras++;
    }
  }
  /* Se ajusta el número de palabras y lineas según corresponda */
  caracteres = caracteres - lineas;
  if (despues_linea) {
    palabras++;
  }
  if (last_c != "\n") {
    lineas++;
  }
  /* Se muestra la información requerida por la guía por archivo */
  cout << "El archivo " << (str -> nombre) << " contiene :" << endl;
  cout << "Lineas: " << lineas << endl;
  cout << "Palabras: " << palabras << endl;
  cout  << "Caracteres:" << caracteres << endl;
  cout << "Caracteres:" << caracteres << endl;
  /* Se cierra el archivo */
  close(stream);
  str -> lineas = lineas;
  str -> palabras = palabras;
  str -> caracteres = caracteres;
  /* Se libera el proceso en la hebra */
  pthread_exit(0);
}

/* Función principal del programa */
int main(int argc, char * argv[]) {
  /* Se declaran variables para guardar los totales que contienen la suma de
  lineas, palabras y caracteres de todos los archivos */
  int lineas_totales = 0, palabras_totales = 0, caracteres_totales = 0;
  int cantidad = argc - 1;
  /* Se crean tantas hebras y archivos como los ingresados por el usuario */
  pthread_t threads[cantidad];
  Archivo files[cantidad];
  /* Se empiza a contar el tiempo de ejecución */
  unsigned ti;
  ti = clock();
  /* En ciclo se incician los proceso de las hebras por cada archivo */
  for (int i = 0; i < cantidad; i++) {
    files[i].nombre = argv[i + 1];
    pthread_create( & threads[i], NULL, abrirArchivo, (void * ) & files[i]);
    pthread_join(threads[i], NULL);
    /* Se suman los totales por archivo */
    lineas_totales += files[i].lineas;
    palabras_totales +=  files[i].palabras;
    palabras_totales += files[i].palabras;
    caracteres_totales += files[i].caracteres;
  }

  /* Se le entrega el resumen de las cantidades al usuario */
  cout << "Resumen de coincidencias entre la totalidad de archivos: " << endl;
  cout << "Lineas: " << lineas_totales << endl;
  cout << "Palabras: " << palabras_totales << endl;
  cout << "Caracteres: " << caracteres_totales << endl;

  unsigned tf;
  tf = clock();
  double tiempo;
  tiempo = (double(tf - ti) / CLOCKS_PER_SEC);
  /* Se le muestra al usuario el tiempo de ejecución del programa */
  cout << "Tiempo empleado en la versión hebra: " << tiempo << endl;
  return 0;
}

