# Secuencial

## Lab 3 - Unidad I - SSOO y redes

## Descripción del programa
El programa mediante el uso de funciones abre una serie de archivos, dados los nombres por el usuario, y cuenta todas las líneas, palabras y caracteres que contiene. 

## Prerrequisitos

### Sistema operativo

* Sistema operativo con el kernel de Linux, Ubuntu o Debian preferentemente

Siempre para instalar o correr programas se recomienda actualizar su máquina mediante el comando desde la terminal 
```
sudo apt update && sudo apt upgrade
```
### Make

Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:
```
make
```
** De falar make

* En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:
```
g++ programa_secuencial.cpp -o programa_secuencial
```
### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando con parámetro de entrada:
```
./programa_secuencial {Archivo1 Archivo2 etc}
```
## Salidas
* Ejemplo ejecuciòn:
```
./programa_secuencial a1.txt a2.txt a3.txt
```
```
El archivo a1.txt contiene :
Lineas: 663
Palabras: 4887
Caracteres:23922
Caracteres:23922
El archivo a2.txt contiene :
Lineas: 717
Palabras: 14109
Caracteres:52121
Caracteres:52121
El archivo a3.txt contiene :
Lineas: 526
Palabras: 9722
Caracteres:36428
Caracteres:36428
Resumen de coincidencias entre la totalidad de archivos: 
Lineas: 1906
Palabras: 57436
Caracteres: 112471
Tiempo empleado en la versión secuencial: 0.080297

```
La salida forzosa del programa en cualquier parte de su ejecución es mediante 
```
ctl+z 
```
o 
```
ctl+c
```
## Construido con
* Lenguaje c++: librería pthread.h, unistd.h, fcntl.h, string.h, iostream, sstream.

## Autor
- Dante Aguirre Solis 
- daguirre12@alumnos.utalca.cl
