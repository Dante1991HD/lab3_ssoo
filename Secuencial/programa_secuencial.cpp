#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

using namespace std;
/* Estrucutura con atributos para el manejo de archivos */
struct Archivo {
  char * nombre;
  int lineas;
  int palabras;
  int caracteres;
};
/* Función recibe el putero al archivo para abrirlo y 
contar las caracteristicas de interes */
void * abrirArchivo(void * txt) {
  /* Se declaran vaariables para el control de la posición de la cuenta 
  en el archivo, se inicializan en 0 */
  Archivo * str = (Archivo * ) txt;
  char c[2], * last_c;
  int lineas = 0, palabras = 0, caracteres = 0;
  bool despues_linea;
  /* Se abre el archivo */
  int stream = open(str -> nombre, O_RDONLY);
  /* Da cuenta de un error al usuario cuando falla al abrir el archivo */
  if (stream == -1) {
    cout << "Error al abrir archivo" << endl;
    /* Se libera el proceso en la hebra */
    pthread_exit(0);
  } else {
    /* Se utulizan ciclos para contar los caracteres, lineas y palabras */
    while (read(stream, c, 1)) {
      caracteres++;
      last_c = c;
      if (despues_linea) {
        palabras++;
        despues_linea = false;
      }
      if (strchr("\n", c[0])) {
        lineas++;
        despues_linea = true;
      }
      if (strchr(" ", c[0])) {
        palabras++;
      }
    }
  }
  /* Se ajusta el número de palabras y lineas según corresponda */
  caracteres = caracteres - lineas;
  if (despues_linea) {
    palabras++;
  }
  if (last_c != "\n") {
    lineas++;
  }
  /* Se muestra la información requerida por la guía por archivo */
  cout << "El archivo " << (str -> nombre) << " contiene :" << endl;
  cout << "Lineas: " << lineas << endl;
  cout << "Palabras: " << palabras << endl;
  cout << "Caracteres:" << caracteres << endl;
  /* Se cierra el archivo */
  close(stream);

  str -> lineas = lineas;
  str -> palabras = palabras;
  str -> caracteres = caracteres;
  return 0;
}
/* Función principal del programa */
int main(int argc, char * argv[]) {
  /* Se declaran variables para guardar los totales que contienen la suma de
  lineas, palabras y caracteres de todos los archivos */
  unsigned ti;
  ti = clock();
  int lineas_totales = 0;
  int palabras_totales = 0;
  int caracteres_totales = 0;

  /* Se crean tantos objetos "Archivo" como los ingresados por el usuario */
  Archivo archivos[argc - 1];

  /* En ciclos se crean y abren los archivos ingresados por el usuario */

  for (int i = 0; i < argc - 1; i++) {
    archivos[i].nombre = argv[i + 1];
  }

  for (int i = 0; i < argc - 1; i++) {
    abrirArchivo((void * ) & archivos[i]);

  }
  /* Se suman los totales por archivo */
  for (int i = 0; i < argc - 1; i++) {
    lineas_totales += archivos[i].lineas;
    palabras_totales += archivos[i].palabras;
    caracteres_totales += archivos[i].caracteres;
  }
  /* Se le entrega el resumen de las cantidades al usuario */
  cout << "Resumen de coincidencias entre la totalidad de archivos: " << endl;
  cout << "Lineas: " << lineas_totales << endl;
  cout << "Palabras: " << palabras_totales << endl;
  cout << "Caracteres: " << caracteres_totales << endl;
  /* Se le muestra al usuario el tiempo de ejecución del programa */
  unsigned tf = clock();
  double tiempo = (double(tf - ti) / CLOCKS_PER_SEC);
  cout << "Tiempo empleado en la versión secuencial: " << tiempo << endl;

  return 0;
}
